#!/bin/bash

#source the OpenFOAM-v2112 environment
source $HOME/OpenFOAM/OpenFOAM-v2112/etc/bashrc

#Parametrize  using pyFoam
./create-study110.py -s test110 -c case -p case.parameter

#intialization of cases
for case in test110_*; do cd $case; ./Allclean; cd ..; done
for case in test110_*; do cd $case; ./Allrun; cd ..; done

#local run
for case in test110_*; do cd $case; interFlow >log.interFlow; cd ..; done

#remote run
#for case in test110_*; do cd $case; sbatch script.sh; cd ..; done


