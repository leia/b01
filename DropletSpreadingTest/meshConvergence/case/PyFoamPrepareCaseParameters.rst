.. title:: RDFDebug_00000_dropletSpreading
.. sectnum::
.. header:: RDFDebug_00000_dropletSpreading
.. header:: Fri May 20 12:42:24 2022
.. footer:: ###Page### / ###Total###

Parameters set in case directory ``/home/asghar/OpenFOAM/B01/debug_test_cases/reconstructionTest/RDFDebug_00000_dropletSpreading`` at *Fri May 20 12:42:24 2022*

.. contents::


************************
Parameters with defaults
************************

Automatically defined values
============================

Short name: ``System``

================== =============================================================================================== ===============================================================================================
..                 default                                                                                         Value                                                                                          
================== =============================================================================================== ===============================================================================================
casePath           "/home/asghar/OpenFOAM/B01/debug_test_cases/reconstructionTest/RDFDebug_00000_dropletSpreading" "/home/asghar/OpenFOAM/B01/debug_test_cases/reconstructionTest/RDFDebug_00000_dropletSpreading"
caseName           "RDFDebug_00000_dropletSpreading"                                                               "RDFDebug_00000_dropletSpreading"                                                              
foamVersion        ()                                                                                              ()                                                                                             
foamFork           openfoam                                                                                        openfoam                                                                                       
numberOfProcessors 1                                                                                               1                                                                                              
================== =============================================================================================== ===============================================================================================

**********************
Unspecified parameters
**********************
If these parameters are actually used then specify them in ``default.parameters``


=========== =============
..          Value        
=========== =============
Umag        1.0          
theta0      50           
STM         fitParaboloid
solver      interFlow    
maxcellSize 0.0001       
courantNo   0.1          
mincellSize 0.0001       
=========== =============
